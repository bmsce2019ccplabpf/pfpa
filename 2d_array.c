#include<stdio.h>
void show_marks(int ,int);
void get_marks(int marks[5][3])
{
    int i,j;
    for(i=0;i<5;i++)
    {
        printf("Enter the marks obtained by student %d.\n",i);
        for(j=0;j<3;j++)
        {
            printf(" for subject[%d]",j);
            scanf("%d",&marks[i][j]);
        }
    }

}
void get_max_marks(int marks[5][3])
{
    int i,j,max_marks;
    for(j=0;j<3;j++)
    {
        max_marks=marks[0][j];
        for(i=0;i<5;i++)
        {
            if (marks[i][j]>max_marks)
                max_marks=marks[i][j];
        }
        show_marks(j,max_marks);
    }
}
void show_marks(int j,int max_marks)
{
    printf("The highest obtained in subject %d=%d.\n",j,max_marks);
}
int main()
{
    int marks[5][3];
    get_marks(marks);
    get_max_marks(marks);
    return 0;
}
