#include<stdio.h>
char input()
{
	char x;
	printf("Enter the character.\n");
	scanf("%c", &x);
	return x;
}

int check(char c)
{

	switch (c) {
		case 'A':
		case 'a':
		case 'E':
		case 'e':
		case 'I':
		case 'i':
		case 'O':
		case 'o':
		case 'U':
		case 'u':
			return 0;
		default:
			if ( c >='B' && c <='z')
				return 1;
			return 2;
	}
}

void output(char x, int n)
{
	if (n == 0)
		printf("%c is a vowel.\n", x);
	else if (n == 1)
		printf("%c is a consonant.\n", x);
	else printf("Error.Enter the character again.\n");
}

int main()
{
	char x;
	x = input();
	int n = check(x);
	output(x, n);
	return 0;
}