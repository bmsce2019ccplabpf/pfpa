#include<stdio.h>
#include<string.h>
void get_string(char str[100])
{
	printf("Enter the string.\n");
	gets(str);
}

char get_newstring(char str1[100],char str2[100],char str3[100])
{
	int i=0,j=0;
	while(str1[i]!='\0')
	{
		str3[j]=str1[i];
		i++;
		j++;
	}
	i=0;
	while(str2[i]!='\0')
	{
		str3[j]=str2[i];
		i++;
		j++;
	}
	str3[j]='\0';
}

void show_string(char str3[100])
{
	printf("The concatenated string is :");
	puts(str3);
}

int main()
{
	char str1[100],str2[100],str3[100];
	get_string(str1);
	get_string(str2);
	get_newstring(str1,str2,str3);
	show_string(str3);
	return 0;
}

