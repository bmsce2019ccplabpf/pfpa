#include<stdio.h>
int input()
{
	int x;
	printf("Enter the number.\n");
	scanf("%d",&x);
	return x;
}
int compute_sum(int n)
{
	int i,sum=0;
	 i=n;
    while(i>0)
    {
        sum+=i%10;
        i=i/10;
    }
    sum+=i;
   return sum;
}
void show_sum(int n,int sum)
{
	printf("The sum of the digits of %d is %d.\n",n,sum);
}
int main()
{
    int n;
    n=input();
    int sum=compute_sum(n);
    show_sum(n,sum);
    return 0;
}
