#include<stdio.h>

void get_numbers(int *a,int *b)
{
    printf("Enter the numbers to be swapped.\n");
    scanf("%d %d",a,b);
}
void show_beforeswapping(int a,int b)
{
    printf("Numbers before swapping:a=%d,b=%d",a,b);
}

void swap(int *i,int *j)
{
    int temp=*i;
    *i=*j;
    *j=temp;
}

void show_afterswapping(int a,int b)
{
    printf("Numbers after swapping:a=%d,b=%d",a,b);
}

int main()
{
    int a,b;
    get_numbers(&a,&b);
    show_beforeswapping(a,b);
    swap(&a,&b);
    show_afterswapping(a,b);
    return 0;
}
