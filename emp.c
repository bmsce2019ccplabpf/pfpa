#include<stdio.h>
struct date
{
    int day;
    int month;
    int year;
};

struct employee
{
    int id_no;
    char name[40];
    int salary;
    struct date doj;
};

typedef struct employee emp;

void show_EmpDetails(emp Emp)
{
    printf("Id is %d.\n",Emp.id_no);
    printf("Employee name is %s",Emp.name);
    printf("Salary of employee is %d.\n",Emp.salary);
    printf("Date of joining is %d.%d.%d.\n",Emp.doj.day,Emp.doj.month,Emp.doj.year);
}

void get_EmpDetails(emp Emp)
{
    printf("Enter the Employee Id.\n");
    scanf("%d",&Emp.id_no);
    printf("Enter the name of the employee.\n");
    scanf("%s",Emp.name);
    printf("Enter the salary.\n");
    scanf("%d",&Emp.salary);
    printf("Enter the date of joining(dd.mm.yyyy).\n");
    scanf("%d.%d.%d",&Emp.doj.day,&Emp.doj.month,&Emp.doj.year);
    show_EmpDetails(Emp);
}
int main()
{
    emp Emp;
    get_EmpDetails(Emp);
    return 0;
}
