#include<stdio.h>
#include<math.h>
void input(int *x, int *y, int *z)
{
	printf("Enter three numbers one by one. \n");
	scanf("%d%d%d",x,y,z);
}
int compute(int x,int y,int z)
{
	if (x>y)
	{
		if(x>z)
			return x;
		else return z;
	}
	else if(y>z)
		return y;
	else return z;
}
void output(int x,int y,int z,int n)
{
	printf("The greatest among %d,%d and %d is %d.\n",x,y,z,n);
}
int main()
{
	int x,y,z;
	input(&x, &y, &z);
	int n=compute(x,y,z);
	output(x,y,z,n);
	return 0;
}