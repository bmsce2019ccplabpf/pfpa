#include<stdio.h>
int main()
{
	char name[50];
	int age;
	FILE *fp;
	fp=fopen("INPUT.txt","w");
	if (fp==NULL)
	{
		printf("File opening failed.\n");
		return -1;
	}
	printf("Enter the name and age.\n");
	scanf("%s%d",name,&age);
	fprintf(fp,"%s %d\n",name,age);
	fclose(fp);
	fp=fopen("INPUT.txt","r");
	if (fp==NULL)
	{
		printf("File opening failed.\n");
		return -1;
	}
	fscanf(fp,"%s %d",name,&age);
	printf("%s %d",name,age);
	fclose(fp);
	return 0;
}