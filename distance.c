#include<stdio.h>
#include<math.h>
void input(float *x,float *y)
{
	printf("Enter the x and y coordinate.\n");
	scanf("%f%f",x,y);
}

float compute_dist(float x,float y,float m,float n)
{
	float dist=sqrt(pow((m-x),2)+pow((n-y),2));
	return dist;
}

void show_dist(float x,float y,float m,float n,float dist)
{
	printf("The distance between the point (%f,%f) and (%f,%f) is %f.\n",x,y,m,n,dist);
}
int main()
{
	float x,y,m,n;
    input(&x,&y);
    input(&m,&n);
    float distance=compute_dist(x,y,m,n);
    show_dist(x,y,m,n,distance);
    return 0; 
}